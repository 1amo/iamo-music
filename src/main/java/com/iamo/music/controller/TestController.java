package com.iamo.music.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ Author：    qiu ji xing
 * @ Date：      2022/1/19 15:40
 * @ Description：   测试控制器
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/say")
    public String sayHello(){
        return "hello world";
    }
}
